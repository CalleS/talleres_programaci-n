﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication6
{
    class Program
    {
        static void Main(string[] args)
        {
            
		double a, b, r;
		int n;
		string temp;
		bool x = true;
		
		do
		{	
			Console.WriteLine("Operaciones matemáticas:");
			Console.WriteLine("Define un valor numérico.");
			temp = Console.ReadLine();
			
			if(double.TryParse(temp,out a))	
		  	 {	
				
			Console.WriteLine("Define otro valor numérico.");
			temp = Console.ReadLine();
		
				if(double.TryParse(temp,out b))
				 {	
					
			Console.WriteLine("¿De qué manera deseas operar los valores?");
			Console.WriteLine("1. Suma");
			Console.WriteLine("2. Resta");
			Console.WriteLine("3. Multiplicación");
			Console.WriteLine("4. División");
			Console.WriteLine("5. Salir");
			temp = Console.ReadLine();
			  			
					if(int.TryParse(temp,out n))
						{
							switch (n)
							{
									case 1:
									r = (a+b);
									Console.WriteLine("La suma entre los números es "+r);
									break;
									
									case 2:
									r = (a-b);
									Console.WriteLine("La resta entre los números es "+r);
									break;
									
									case 3:
									r = (a*b);
									Console.WriteLine("La multiplicacion entre los números es "+r);
									break;
									
									case 4:
									if(b != 0){
										r = (a/b);
										Console.WriteLine("La division entre los números es "+r);
									}
									else{
										Console.WriteLine("No es posible dividir por cero.");
									}
									break;
									
									case 5:
									Console.WriteLine("Digitó esos números para nada pero bueno, chao.");
									x = false;
									break;
									
									default:
									Console.WriteLine("Era un número entre uno y cinco...");
									break;
							}
						} else {Console.WriteLine("¡Valores numéricos por favor!");}
				} else { Console.WriteLine("¡Valores numéricos por favor!"); }
			} else { Console.WriteLine("¡Valores numéricos por favor!"); }
		}
		while(x!=false);
            Console.ReadKey();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication11
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] porcentajes = { 0.06, 0.06, 0.12, 0.12, 0.12, 0.12, 0.2, 0.2 };
            double[] notas = new double[porcentajes.Length];
            string temp;
            int ini = 0;
            double promedio = 0.0, nota;

            Console.WriteLine("Promedio para la nota final de fundamentos de programación con Xaca Rana");
            Console.Write("\n");
            while (ini < porcentajes.Length)
            {
                Console.WriteLine("Ingrese la nota " + ini + ", corresponde al " + (100 * porcentajes[ini]) + "% de la materia.");
                temp = Console.ReadLine();

                if (double.TryParse(temp, out nota))
                {
                    if (nota >= 0 && nota <= 5)
                    {
                        notas[ini] = nota;
                        ini += 1;
                    }
                    else { Console.WriteLine("La nota debe ser un valor entre 0 y 5"); }
                }
                else { Console.WriteLine("Error al ingresar la nota"); }
            }
            for (ini = 0; ini < notas.Length; ini++)
            {
                promedio += notas[ini] * porcentajes[ini];
            }

            if (Math.Round((promedio / notas.Length) * 10, 1) < 3)
            {
                Console.WriteLine("Lo siento mucho, tu nota final es " + Math.Round((promedio / notas.Length) * 10, 1));
            }
            else { Console.WriteLine("Lo lograste, tu nota final es " + Math.Round((promedio / notas.Length) * 10, 1)); }
            Console.ReadKey();
        }
    }
}

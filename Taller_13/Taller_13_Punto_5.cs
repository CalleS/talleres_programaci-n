﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication12
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random((int)DateTime.Now.Ticks);

            Console.WriteLine("Ruleta de la muerte");
            Console.WriteLine("Si te sale un color frío (azul, violeta, verde), perderás programación; pasa lo opuesto con colores cálidos (rojo, naranja, amarillo).");

            Console.Write("\n");
            string[] palabras = { "Azul", "Violeta", "Verde", "Rojo", "Naranja", "Amarillo" };
            string temp = palabras[rnd.Next(0, palabras.Length)];

            Console.WriteLine(temp);
            Console.ReadKey();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication10
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, m;
            string temp;

            Console.WriteLine("Números desde 0 hasta M");
            Console.Write("\n");
            Console.WriteLine("Ingrese M");
            temp = Console.ReadLine();

            if (int.TryParse(temp, out m))
            {

                Console.WriteLine("Presiona 1 para ver los números de forma ascendiente.");
                Console.WriteLine("Presiona 2 para ver los números de forma descendente.");
                temp = Console.ReadLine();

                if (int.TryParse(temp, out a))
                {
                    if (a < 3 && a > 0)
                    {
                        if (a == 1)
                        {
                            Console.Write("[");
                            for (int i = 0; i < m + 1; i++)
                            {
                                Console.Write(i);
                                if (i != m)
                                {
                                    Console.Write(", ");
                                }
                            }
                            Console.WriteLine("]");
                        }
                        if (a == 2)
                        {
                            Console.Write("[");
                            for (int i = m; i >= 0; i--)
                            {
                                Console.Write(i);
                                if (i != 0)
                                {
                                    Console.Write(", ");
                                }
                            }
                            Console.WriteLine("]");
                        }
                    }
                    else { Console.WriteLine("Valor no válido"); }
                }
            }
            else { Console.WriteLine("Valor no válido"); }
            Console.ReadKey();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication13
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ingrese una palabra para verificar si es palíndroma.");
            string palab = Console.ReadLine();
            string palin = "";
            for (int i = palab.Length - 1; i >= 0; i--)
            {
                palin += (palab[i]);
            }

            if (palab.Equals(palin))
            {
                Console.WriteLine("La palabra es palíndroma: " + palab + " es lo mismo que " + palin);
            }
            else { Console.WriteLine("La palabra no es palíndroma: " + palab + " no es lo mismo que " + palin); }
            Console.ReadKey();
        }
    }
}

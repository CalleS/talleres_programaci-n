﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class Program
    {
        static void Main(string[] args)
        {
            // Decreciente
            int a, b, x;
            string temp;

            Console.WriteLine("Imprimir números de manera decreciente, desde M hasta N");
            Console.WriteLine("Ingrese un número entero positivo.");
            temp = Console.ReadLine();

            if (int.TryParse(temp, out a))

            {
                Console.WriteLine("Ingrese otro número, menor que el ingresado anteriormente.");
                temp = Console.ReadLine();

                if (int.TryParse(temp, out b))
                {
                    if (a > b)

                    {
                        for (x = a; x >= b; x--)
                        {
                            Console.WriteLine(x);
                        }
                    }
                    else { Console.WriteLine("¿No entiende que menor?"); }

                }
                else { Console.WriteLine("No pana, valor no válido."); }

            }
            else { Console.WriteLine("No pana, valor no válido."); }
            Console.ReadKey();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication14
{
    class Program
    {
        static void Main(string[] args)
        {
            string temp;
            int a, b;

            do
            {
                Console.WriteLine("                                                        &&&");
                Console.WriteLine("        CCCCCCCCCCCCC                  lllllll lllllll                             SSSSSSSSSSSSSSS ");
                Console.WriteLine("     CCC::::::::::::C                  l:::::l l:::::l                           SS:::::::::::::::S");
                Console.WriteLine("   CC:::::::::::::::C                  l:::::l l:::::l                          S:::::SSSSSS::::::S");
                Console.WriteLine("  C:::::CCCCCCCC::::C                  l:::::l l:::::l                          S:::::S     SSSSSSS");
                Console.WriteLine(" C:::::C       CCCCCC  aaaaaaaaaaaaa    l::::l  l::::l     eeeeeeeeeeee         S:::::S            ");
                Console.WriteLine("C:::::C                a::::::::::::a   l::::l  l::::l   ee::::::::::::ee       S:::::S            ");
                Console.WriteLine("C:::::C                aaaaaaaaa:::::a  l::::l  l::::l  e::::::eeeee:::::ee      S::::SSSS         ");
                Console.WriteLine("C:::::C                         a::::a  l::::l  l::::l e::::::e     e:::::e       SS::::::SSSSS    ");
                Console.WriteLine("C:::::C                  aaaaaaa:::::a  l::::l  l::::l e:::::::eeeee::::::e         SSS::::::::SS  ");
                Console.WriteLine("C:::::C                aa::::::::::::a  l::::l  l::::l e:::::::::::::::::e             SSSSSS::::S ");
                Console.WriteLine("C:::::C               a::::aaaa::::::a  l::::l  l::::l e::::::eeeeeeeeeee                   S:::::S");
                Console.WriteLine(" C:::::C       CCCCCCa::::a    a:::::a  l::::l  l::::l e:::::::e                            S:::::S");
                Console.WriteLine("  C:::::CCCCCCCC::::Ca::::a    a:::::a l::::::ll::::::le::::::::e               SSSSSSS     S:::::S");
                Console.WriteLine("   CC:::::::::::::::Ca:::::aaaa::::::a l::::::ll::::::l e::::::::eeeeeeee       S::::::SSSSSS:::::S");
                Console.WriteLine("     CCC::::::::::::C a::::::::::aa:::al::::::ll::::::l  ee:::::::::::::e       S:::::::::::::::SS ");
                Console.WriteLine("        CCCCCCCCCCCCC  aaaaaaaaaa  aaaallllllllllllllll    eeeeeeeeeeeeee        SSSSSSSSSSSSSSS   ");
                Console.WriteLine("");
                Console.WriteLine("                                          _______,.........._");
                Console.WriteLine("                                     _.::::::::::::::::::::::::._");
                Console.WriteLine("                                  _J::::::::::::::::::::::::::::::-.");
                Console.WriteLine("                               _,J::::;::::::!:::::::::::!:::::::::::-.\"\\_ ___");
                Console.WriteLine("                            ,-:::::/::::::::::::/''''''-:/   \\::::::::::::::::L_");
                Console.WriteLine("                          ,;;;;;::!::/         V               -::::::::::::::::7");
                Console.WriteLine("                        ,J:::::::/ \\/                              '-'`\\:::::::.7");
                Console.WriteLine("                        |:::::::'                                       \\::!:::/");
                Console.WriteLine("                       J::::::/                                          `.!:\\ dp");
                Console.WriteLine("                       |:::::7                                             |/\\:\\");
                Console.WriteLine("                      J::::/                                               \\/ \\:|");
                Console.WriteLine("                      |:::/                                                    \\:\\");
                Console.WriteLine("                      |::/                                                     |:.Y");
                Console.WriteLine("                      |::7                                                      |:|");
                Console.WriteLine("                      |:/                              `OOO8ooo._               |:|");
                Console.WriteLine("                      |/               ,,oooo8OO'           `\"`Y8o,             |'");
                Console.WriteLine("                       |            ,odP\"'                      `8P            /");
                Console.WriteLine("                       |          ,8P'    _.__         .---.._                /");
                Console.WriteLine("                       |           '   .-'    `-.    .'       `-.            /");
                Console.WriteLine("                       `.            ,'          `. /            `.          L_");
                Console.WriteLine("                     .-.J.          /              Y               \\        /_ \\");
                Console.WriteLine("                    |    \\         /               |                Y      // `|");
                Console.WriteLine("                     \\ '\\ \\       Y          8B    |   8B           |     /Y   '");
                Console.WriteLine("                      \\  \\ \\      |                |                ;    / |  /");
                Console.WriteLine("                       \\  \\ \\     |               ,'.              /    /  L |");
                Console.WriteLine("                        \\  J \\     \\           _.'   `-._       _.'    /  _.'");
                Console.WriteLine("                         `.___,\\    `._     _,'          '-...-'     /'`\"'");
                Console.WriteLine("                                \\      '---'  _____________         /");
                Console.WriteLine("                                 `.           \\|T T T T T|/       ,'");
                Console.WriteLine("                                   `.           \\_|_|_|_/       .'");
                Console.WriteLine("                                     `.         `._.-..'      .'");
                Console.WriteLine("                                       `._         `-'     _,'");
                Console.WriteLine("                                          `--._________.--'");

                Console.Write("\n");
                Console.WriteLine("1. Partida nueva.");
                Console.WriteLine("2. Tabla de posiciones.");
                Console.WriteLine("3. Salir.");

                temp = Console.ReadLine();

                if (int.TryParse(temp, out a))
                {

                    switch (a)
                    {

                        case 1:
                            do
                            {
                                Console.Write("\n");
                                Console.WriteLine("     ___       __    __    ______   .______        ______     ___       _______   __  .___________.  ______   ");
                                Console.WriteLine("    /   \\     |  |  |  |  /  __  \\  |   _  \\      /      |   /   \\     |       \\ |  | |           | /  __  \\  ");
                                Console.WriteLine("   /  ^  \\    |  |__|  | |  |  |  | |  |_)  |    |  ,----'  /  ^  \\    |  .--.  ||  | `---|  |----`|  |  |  | ");
                                Console.WriteLine("  /  /_\\  \\   |   __   | |  |  |  | |      /     |  |      /  /_\\  \\   |  |  |  ||  |     |  |     |  |  |  | ");
                                Console.WriteLine(" /  _____  \\  |  |  |  | |  `--'  | |  |\\  \\----.|  `----./  _____  \\  |  '--'  ||  |     |  |     |  `--'  | ");
                                Console.WriteLine("/__/     \\__\\ |__|  |__|  \\______/  | _| `._____| \\______/__/     \\__\\ |_______/ |__|     |__|      \\______/  ");
                                Console.Write("\n");

                                Console.WriteLine("");
                                Console.WriteLine("1. Seleccionar categoria (5 categorias disponibles).");
                                Console.WriteLine("2. Categoria aleatoria.");
                                Console.WriteLine("3. Salir.");
                                temp = Console.ReadLine();

                                if (int.TryParse(temp, out a))
                                {
                                    switch (a)
                                    {
                                        case 1:
                                            Console.WriteLine("Seleccione la categoria de preferencia");
                                            Console.WriteLine("1. Ropa");
                                            Console.WriteLine("2. Deportes");
                                            Console.WriteLine("3. Sistema solar");
                                            Console.WriteLine("4. Alcohol");
                                            Console.WriteLine("5. Generos musicales");
                                            temp = Console.ReadLine();
                                            if (int.TryParse(temp, out b))
                                            {
                                                switch (b)
                                                {
                                                    case 1:
                                                        string[] palabras =
                                                {"Chanclas", "Sudadera", "Sombrero", "Gafas", "Zapatos", "Gorra", "Jean", "Camisilla", "Medias", "Calzones"};

                                                        Console.WriteLine("Ingrese un numero entre 0 y 9");
                                                        string palabra = Console.ReadLine();
                                                        int numeropalabra;
                                                        if (int.TryParse(palabra, out numeropalabra))
                                                        {
                                                            if (numeropalabra < palabras.Length)
                                                            {
                                                                Console.WriteLine(palabras[numeropalabra]);
                                                            }
                                                            else { Console.WriteLine("La Posicion esta fuera de rango"); }
                                                        }
                                                        Console.Write("\n");
                                                        Console.WriteLine("Ingrese la palabra que le tocó.");
                                                        string m = Console.ReadLine();
                                                        Console.WriteLine(" la longitud de la palabra es: " + m.Length);
                                                        string a_b = "_ ";
                                                        string n = "";
                                                        for (int i = 0; i < m.Length; i++)
                                                        {
                                                            n += a_b;
                                                        }
                                                        n.Remove(n.Length - 1);
                                                        Console.WriteLine(n);

                                                        break;
                                                    case 2:
                                                        string[] palabras2 =
                                                {"Atletismo", "MMA", "Natación", "Surf", "Raskingball", "Tennis", "Golf", "Boxeo", "Futbol", "Basketball"};

                                                        Console.WriteLine("Ingrese un numero entre 0 y 9");
                                                        string palabra2 = Console.ReadLine();
                                                        int numeropalabra2;
                                                        if (int.TryParse(palabra2, out numeropalabra2))
                                                        {
                                                            if (numeropalabra2 < palabras2.Length)
                                                            {
                                                                Console.WriteLine(palabras2[numeropalabra2]);
                                                            }
                                                            else { Console.WriteLine("La Posicion esta fuera de rango"); }
                                                        }
                                                        Console.Write("\n");
                                                        Console.WriteLine("Ingrese la palabra que le tocó.");
                                                        string mn = Console.ReadLine();
                                                        Console.WriteLine(" la longitud de la palabra es: " + mn.Length);
                                                        string m_n = "_ ";
                                                        string nm = "";
                                                        for (int i = 0; i < mn.Length; i++)
                                                        {
                                                            nm += m_n;
                                                        }
                                                        nm.Remove(nm.Length - 1);
                                                        Console.WriteLine(nm);

                                                        break;
                                                    case 3:
                                                        string[] palabras3 =
                                                {"Sol", "Mercurio", "Venus", "Tierra", "Marte", "Júpiter", "Saturno", "Urano", "Neptuno", "Pluto"};

                                                        Console.WriteLine("Ingrese un numero entre 0 y 9");
                                                        string palabra3 = Console.ReadLine();
                                                        int numeropalabra3;
                                                        if (int.TryParse(palabra3, out numeropalabra3))
                                                        {
                                                            if (numeropalabra3 < palabras3.Length)
                                                            {
                                                                Console.WriteLine(palabras3[numeropalabra3]);
                                                            }
                                                            else { Console.WriteLine("La Posicion esta fuera de rango"); }
                                                        }
                                                        Console.Write("\n");
                                                        Console.WriteLine("Ingrese la palabra que le tocó.");
                                                        string on = Console.ReadLine();
                                                        Console.WriteLine(" la longitud de la palabra es: " + on.Length);
                                                        string o_n = "_ ";
                                                        string no = "";
                                                        for (int i = 0; i < on.Length; i++)
                                                        {
                                                            no += o_n;
                                                        }
                                                        no.Remove(no.Length - 1);
                                                        Console.WriteLine(no);

                                                        break;
                                                    case 4:
                                                        string[] palabras4 =
                                                {"Vodka", "Ron", "Aguardiente", "Tequila", "Brandy", "Vino", "Cerveza", "Whisky", "Ginebra", "Etílico"};

                                                        Console.WriteLine("Ingrese un numero entre 0 y 9");
                                                        string palabra4 = Console.ReadLine();
                                                        int numeropalabra4;
                                                        if (int.TryParse(palabra4, out numeropalabra4))
                                                        {
                                                            if (numeropalabra4 < palabras4.Length)
                                                            {
                                                                Console.WriteLine(palabras4[numeropalabra4]);
                                                            }
                                                            else { Console.WriteLine("La Posicion esta fuera de rango"); }
                                                        }
                                                        Console.Write("\n");
                                                        Console.WriteLine("Ingrese la palabra que le tocó.");
                                                        string ol = Console.ReadLine();
                                                        Console.WriteLine(" la longitud de la palabra es: " + ol.Length);
                                                        string o_l = "_ ";
                                                        string lo = "";
                                                        for (int i = 0; i < ol.Length; i++)
                                                        {
                                                            lo += o_l;
                                                        }
                                                        lo.Remove(lo.Length - 1);
                                                        Console.WriteLine(lo);

                                                        break;

                                                    case 5:
                                                        string[] palabras5 =
                                                {"Metal", "Rock", "Reggae", "Dancehall", "Dubstep", "Techno", "Indie", "Experimental", "Blues", "Jazz"};

                                                        Console.WriteLine("Ingrese un numero entre 0 y 9");
                                                        string palabra5 = Console.ReadLine();
                                                        int numeropalabra5;
                                                        if (int.TryParse(palabra5, out numeropalabra5))
                                                        {
                                                            if (numeropalabra5 < palabras5.Length)
                                                            {
                                                                Console.WriteLine(palabras5[numeropalabra5]);
                                                            }
                                                            else { Console.WriteLine("La Posicion esta fuera de rango"); }
                                                        }
                                                        Console.Write("\n");
                                                        Console.WriteLine("Ingrese la palabra que le tocó.");
                                                        string op = Console.ReadLine();
                                                        Console.WriteLine(" la longitud de la palabra es: " + op.Length);
                                                        string o_p = "_ ";
                                                        string po = "";
                                                        for (int i = 0; i < op.Length; i++)
                                                        {
                                                            po += o_p;
                                                        }
                                                        po.Remove(po.Length - 1);
                                                        Console.WriteLine(po);

                                                        break;
                                                    default:
                                                        Console.WriteLine("Opcion no válida");
                                                        break;
                                                }
                                            }
                                            break;
                                        case 2:
                                            Random szs = new Random();
                                            string[] pra = { "1. Ropa", "2. Deportes", "3. Sistema solar", "4. Alcohol", "5. Generos musicales" };
                                            int altr;
                                            altr = szs.Next(0, pra.Length - 1);
                                            {
                                                switch (altr)
                                                {
                                                    case 1:
                                                        string[] palabras =
                                                {"Chanclas", "Sudadera", "Sombrero", "Gafas", "Zapatos", "Gorra", "Jean", "Camisilla", "Medias", "Calzones"};

                                                        Console.WriteLine("Ingrese un numero entre 0 y 9");
                                                        string palabra = Console.ReadLine();
                                                        int numeropalabra;
                                                        if (int.TryParse(palabra, out numeropalabra))
                                                        {
                                                            if (numeropalabra < palabras.Length)
                                                            {
                                                                Console.WriteLine(palabras[numeropalabra]);
                                                            }
                                                            else { Console.WriteLine("La Posicion esta fuera de rango"); }
                                                        }
                                                        Console.Write("\n");
                                                        Console.WriteLine("Ingrese la palabra que le tocó.");
                                                        string m = Console.ReadLine();
                                                        Console.WriteLine(" la longitud de la palabra es: " + m.Length);
                                                        string a_b = "_ ";
                                                        string n = "";
                                                        for (int i = 0; i < m.Length; i++)
                                                        {
                                                            n += a_b;
                                                        }
                                                        n.Remove(n.Length - 1);
                                                        Console.WriteLine(n);

                                                        break;
                                                    case 2:
                                                        string[] palabras2 =
                                                {"Atletismo", "MMA", "Natación", "Surf", "Raskingball", "Tennis", "Golf", "Boxeo", "Futbol", "Basketball"};

                                                        Console.WriteLine("Ingrese un numero entre 0 y 9");
                                                        string palabra2 = Console.ReadLine();
                                                        int numeropalabra2;
                                                        if (int.TryParse(palabra2, out numeropalabra2))
                                                        {
                                                            if (numeropalabra2 < palabras2.Length)
                                                            {
                                                                Console.WriteLine(palabras2[numeropalabra2]);
                                                            }
                                                            else { Console.WriteLine("La Posicion esta fuera de rango"); }
                                                        }
                                                        Console.Write("\n");
                                                        Console.WriteLine("Ingrese la palabra que le tocó.");
                                                        string mn = Console.ReadLine();
                                                        Console.WriteLine(" la longitud de la palabra es: " + mn.Length);
                                                        string m_n = "_ ";
                                                        string nm = "";
                                                        for (int i = 0; i < mn.Length; i++)
                                                        {
                                                            nm += m_n;
                                                        }
                                                        nm.Remove(nm.Length - 1);
                                                        Console.WriteLine(nm);
                                                        break;
                                                    case 3:
                                                        string[] palabras3 =
                                                {"Sol", "Mercurio", "Venus", "Tierra", "Marte", "Júpiter", "Saturno", "Urano", "Neptuno", "Pluto"};

                                                        Console.WriteLine("Ingrese un numero entre 0 y 9");
                                                        string palabra3 = Console.ReadLine();
                                                        int numeropalabra3;
                                                        if (int.TryParse(palabra3, out numeropalabra3))
                                                        {
                                                            if (numeropalabra3 < palabras3.Length)
                                                            {
                                                                Console.WriteLine(palabras3[numeropalabra3]);
                                                            }
                                                            else { Console.WriteLine("La Posicion esta fuera de rango"); }
                                                        }
                                                        Console.Write("\n");
                                                        Console.WriteLine("Ingrese la palabra que le tocó.");
                                                        string on = Console.ReadLine();
                                                        Console.WriteLine(" la longitud de la palabra es: " + on.Length);
                                                        string o_n = "_ ";
                                                        string no = "";
                                                        for (int i = 0; i < on.Length; i++)
                                                        {
                                                            no += o_n;
                                                        }
                                                        no.Remove(no.Length - 1);
                                                        Console.WriteLine(no);

                                                        break;
                                                    case 4:
                                                        string[] palabras4 =
                                                {"Vodka", "Ron", "Aguardiente", "Tequila", "Brandy", "Vino", "Cerveza", "Whisky", "Ginebra", "Etílico"};

                                                        Console.WriteLine("Ingrese un numero entre 0 y 9");
                                                        string palabra4 = Console.ReadLine();
                                                        int numeropalabra4;
                                                        if (int.TryParse(palabra4, out numeropalabra4))
                                                        {
                                                            if (numeropalabra4 < palabras4.Length)
                                                            {
                                                                Console.WriteLine(palabras4[numeropalabra4]);
                                                            }
                                                            else { Console.WriteLine("La Posicion esta fuera de rango"); }
                                                        }
                                                        Console.Write("\n");
                                                        Console.WriteLine("Ingrese la palabra que le tocó.");
                                                        string ol = Console.ReadLine();
                                                        Console.WriteLine(" la longitud de la palabra es: " + ol.Length);
                                                        string o_l = "_ ";
                                                        string lo = "";
                                                        for (int i = 0; i < ol.Length; i++)
                                                        {
                                                            lo += o_l;
                                                        }
                                                        lo.Remove(lo.Length - 1);
                                                        Console.WriteLine(lo);

                                                        break;
                                                    case 5:
                                                        string[] palabras5 =
                                                {"Metal", "Rock", "Reggae", "Dancehall", "Dubstep", "Techno", "Indie", "Experimental", "Blues", "Jazz"};

                                                        Console.WriteLine("Ingrese un numero entre 0 y 9");
                                                        string palabra5 = Console.ReadLine();
                                                        int numeropalabra5;
                                                        if (int.TryParse(palabra5, out numeropalabra5))
                                                        {
                                                            if (numeropalabra5 < palabras5.Length)
                                                            {
                                                                Console.WriteLine(palabras5[numeropalabra5]);
                                                            }
                                                            else { Console.WriteLine("La Posicion esta fuera de rango"); }
                                                        }
                                                        Console.Write("\n");
                                                        Console.WriteLine("Ingrese la palabra que le tocó.");
                                                        string op = Console.ReadLine();
                                                        Console.WriteLine(" la longitud de la palabra es: " + op.Length);
                                                        string o_p = "_ ";
                                                        string po = "";
                                                        for (int i = 0; i < op.Length; i++)
                                                        {
                                                            po += o_p;
                                                        }
                                                        po.Remove(po.Length - 1);
                                                        Console.WriteLine(po);

                                                        break;
                                                    default:
                                                        Console.WriteLine("Opcion no válida");
                                                        break;


                                                }
                                            }
                                            break;
                                        case 3:
                                            Console.WriteLine("Salida al menú principal");
                                            break;
                                        default:
                                            Console.WriteLine("Opción no válida");
                                            break;

                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Opción no válida");
                                }

                            } while (a != 3);
                            a = 6;
                            break;

                        case 2:
                            break;

                        case 3:
                            Console.WriteLine("Hasta luego, ¡Vuelve pronto!");
                            break;

                        default:
                            Console.WriteLine("Opción no válida");
                            break;

                    }
                }
                else
                {
                    Console.WriteLine("Opción no válida");

                }

            } while (a != 3);
            Console.ReadKey();
        }
    }
}
